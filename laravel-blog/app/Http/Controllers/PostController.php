<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// to access with the authenticated user.
use Illuminate\Support\Facades\Auth;

// to have access with queries related to the Post Entity/Model.
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');
    }

    // action to received the form data and subsequently store said data in the post table.
    public function store(Request $request){
        // if there is an unauthenticated user
        if(Auth::user()){
            // instantiate a new post object from the Post model.
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->isActive = true;
            $post->user_id = (Auth::user()->id);
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    public function index(){
        $posts = Post::all()->where('isActive', true);
        // The "with()" method will allows us to pass information from teh controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome(){
        $posts = Post::inRandomOrder()->where('isActive', true)->limit(3)->get();
        // The "with()" method will allows us to pass information from teh controller to view page
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts->where('isActive', true);

            return view('posts.index')->with('posts', $posts);
        } else {
            return redirect('/login');
        }
    }

    public function show($id){
        $post = Post::find($id);
        if($post->isActive){
            return view('posts.show')->with('post', $post);
        } else {
            return redirect('/posts');
        }
        
    }

    public function edit($id){
        if(Auth::user()){
            $post = Post::find($id);
            if(Auth::user()->id == $post->user_id){
                if($post->isActive){
                    return view('posts.edit')->with('post', $post);
                } else {
                    return redirect('/posts');
                }
            } else {

                return redirect('/posts');
            }
        } else {

            return redirect('/login');
        }     
    }

    public function update(Request $request){

        $post = Post::find($request->id);
        $post->title = $request->title;
        $post->content = $request->content;
        $post->isActive = true;
        $post->user_id = (Auth::user()->id);
        $post->save();

        return redirect('/posts');
    }

    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }
}
